package cliente.Servicios;

import java.util.ArrayList;
import java.util.List;

import persistencia.DAO.ClaseDAO;
import persistencia.DAO.DAOAbstractFactory;
import cliente.domainModel.Alumno;
import cliente.domainModel.Clase;
import cliente.domainModel.Tema;

public class ClaseService 
{
	private ClaseDAO dao; 
	private Clase ultimoPersistido;
	
	public ClaseService(DAOAbstractFactory metodo_persistencia) 
	{
		dao = metodo_persistencia.createClaseDAO();
	}
	
	public void crearClase (boolean isClaseGrupal, String descripcion, ArrayList<Tema> temas, ArrayList<Alumno> alumnos)
	{
		Clase clase = new Clase(isClaseGrupal, descripcion);
		clase.setId(obtenerNuevoId());
		agregarAlumnos(clase, alumnos);
		clase.setAlumnos(alumnos);
		clase.setTemas(temas);
		guardar(clase);
	}
	
	private void agregarAlumnos(Clase clase, ArrayList<Alumno> alumnos) 
	{
		for (Alumno alumno : alumnos) 
		{
			alumno.getClasesAsistidas().add(clase);
		}
	}

	public Clase getClasePorId(int id)
	{
		return dao.getClasePorId(id);
	}
	
	public ArrayList<Clase> getClases()
	{
		return (ArrayList<Clase>) dao.readAll();
	}
	
	private void guardar(Clase c)
	{
		ultimoPersistido = dao.insert(c);
	}
	
	public Clase getUltimoPersistido()
	{
		return ultimoPersistido;
	}
	
	private long obtenerNuevoId()
	{
		int ret = 0;
		try 
		{
			List<Clase> clases = getClases();
			Long ultimoId = clases.get(clases.size()-1).getId();
			ret = (int) (ultimoId + 1);
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return ret;
		
		
	}
	
}
