package cliente.Servicios;

import java.util.ArrayList;
import java.util.List;

import cliente.domainModel.Alumno;
import cliente.domainModel.NivelDeEstudio;
import persistencia.DAO.AlumnoDAO;
import persistencia.DAO.DAOAbstractFactory;

public class AlumnoService 
{
	private AlumnoDAO dao;
	private Alumno ultimoPersistido;
	
	public AlumnoService(DAOAbstractFactory metodo_persistencia) 
	{
		dao = metodo_persistencia.createAlumnoDAO();
	}
	
	public void crearAlumno(String nombre, String telefono, String mail,NivelDeEstudio nivel, 
			String institucionEducativa, String ciudadDeRecidencia, String descripcion)
	{
		Alumno a = new Alumno(nombre, telefono, mail, nivel, institucionEducativa, ciudadDeRecidencia, descripcion);
		a.setId(obtenerNuevoId());
		guardar(a);
	}

	public void guardar(Alumno a) 
	{
		ultimoPersistido = dao.insert(a);
	}
	
	public Alumno getAlumnoPorId(int id)
	{
		return dao.getAlumnoPorId(id);
	}
	
	
	public List<Alumno> getAlumnos()
	{
		return dao.readAll();
	}
	
	public Alumno getUltimoPersistido()
	{
		return ultimoPersistido;
	}
	
	private long obtenerNuevoId()
	{
		int ret = 0;
		try 
		{
			List<Alumno> alumnos = getAlumnos();
			Long ultimoId = alumnos.get(alumnos.size()-1).getId();
			ret = (int) (ultimoId + 1);
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return ret;
		
		
	}
}
