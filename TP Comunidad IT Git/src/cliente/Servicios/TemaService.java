package cliente.Servicios;

import java.util.ArrayList;
import java.util.List;

import cliente.domainModel.Alumno;
import cliente.domainModel.Tema;
import persistencia.DAO.DAOAbstractFactory;
import persistencia.DAO.TemaDAO;

public class TemaService 
{
	private TemaDAO dao;
	private Tema ultimoPersistido;
	
	public TemaService(DAOAbstractFactory metodo_persistencia) 
	{
		dao = metodo_persistencia.createTemaDAO();
	}
	
	public void crearTema(String nombre, String descripcion)
	{
		Tema tema = new Tema(nombre, descripcion);
		tema.setId(obtenerNuevoId());
		guardar(tema);
	}

	private void guardar(Tema tema) 
	{
		ultimoPersistido = dao.insert(tema);
	}
	
	public Tema getTemaPorId(int id)
	{
		return dao.getTemaPorId(id);
	}
	
	public ArrayList<Tema> getTemas()
	{
		return (ArrayList<Tema>) dao.readAll();
	}
	
	public Tema getUltimoPersistido()
	{
		return ultimoPersistido;
	}
	
	private long obtenerNuevoId()
	{
		int ret = 0;
		try 
		{
			List<Tema> temas = getTemas();
			Long ultimoId = temas.get(temas.size()-1).getId();
			ret = (int) (ultimoId + 1);
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		return ret;
	}
}
