package cliente.domainModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Alumno implements Serializable
{
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private String nombre;
	private String telefono;
	private String mail;
	private String nivel;
	private String institucionEducativa;
	private String ciudadDeRecidencia;
	private String descripcion;
	
	private List<Clase> clasesAsistidas;
	private static int incrementalId;
	
	public Alumno()
	{
	}
	
	public Alumno(long id, String nombre, String telefono, String mail,String nivel, 
			String institucionEducativa, String ciudadDeRecidencia, String descripcion) 
	{
		this.id = id;
		this.nombre = nombre;
		this.telefono = telefono;
		this.mail = mail;
		this.nivel = nivel;
		this.institucionEducativa = institucionEducativa;
		this.ciudadDeRecidencia = ciudadDeRecidencia;
		this.descripcion = descripcion;
		this.clasesAsistidas = new ArrayList<Clase>();
	}
	
	public Alumno(String nombre, String telefono, String mail, NivelDeEstudio nivel, 
			String institucionEducativa, String ciudadDeRecidencia, String descripcion) 
	{
		this.nombre = nombre;
		this.telefono = telefono;
		this.mail = mail;
		this.nivel = nivel.toString();
		this.institucionEducativa = institucionEducativa;
		this.ciudadDeRecidencia = ciudadDeRecidencia;
		this.descripcion = descripcion;
		this.clasesAsistidas = new ArrayList<Clase>();
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getNivel() {
		return nivel.toString();
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getInstitucionEducativa() {
		return institucionEducativa;
	}

	public void setInstitucionEducativa(String institucionEducativa) {
		this.institucionEducativa = institucionEducativa;
	}

	public String getCiudadDeRecidencia() {
		return ciudadDeRecidencia;
	}

	public void setCiudadDeRecidencia(String ciudadDeRecidencia) {
		this.ciudadDeRecidencia = ciudadDeRecidencia;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public List<Clase> getClasesAsistidas() {
		return clasesAsistidas;
	}

	public void setClasesAsistidas(ArrayList<Clase> clasesAsistidas) {
		this.clasesAsistidas = clasesAsistidas;
	}
	
	public static int getIncrementalId() {
		return incrementalId;
	}

	public static void setIncrementalId(int incrementalId) {
		Alumno.incrementalId = incrementalId;
	}

	@Override
	public boolean equals(Object a)
	{
		Alumno alumno = (Alumno) a;
		boolean mismoNombre = alumno.getNombre().equals(this.nombre);
		boolean mismoTelefono = alumno.getTelefono().equals(this.telefono);
		boolean mismoMail = alumno.getMail().equals(this.mail);
		return  mismoNombre && mismoTelefono && mismoMail;
	}

	@Override
	public String toString() 
	{
		return "\nID: " + id + "\nNOMBRE: " + nombre + "\nTELEFONO: "
				+ telefono + "\nMAIL: " + mail + "\nDESCRIPCION: " + descripcion;
	}
	
}
