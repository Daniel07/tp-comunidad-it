package cliente.domainModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Clase implements Serializable
{
	private static final long serialVersionUID = 1L;
	private long id;
	private String fecha;
	private boolean isClaseGrupal;
	private String descripcion;
	private List<Tema> temas;
	private List<Alumno> alumnos;
	private static int incrementalId;
	
	public Clase()
	{
	}
	
	public Clase(long id, String fecha, boolean isClaseGrupal, String descripcion) 
	{
		this.isClaseGrupal = isClaseGrupal;
		this.descripcion = descripcion;
		this.temas = new ArrayList<Tema>();
		this.alumnos = new ArrayList<Alumno>();
		this.fecha = fecha;
		this.id = id; 
	}
	
	public Clase(boolean isClaseGrupal, String descripcion) 
	{
		this.isClaseGrupal = isClaseGrupal;
		this.descripcion = descripcion;
		this.temas = new ArrayList<Tema>();
		this.alumnos = new ArrayList<Alumno>();
		this.fecha = new Date().toString();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isClaseGrupal() {
		return isClaseGrupal;
	}

	public void setClaseGrupal(boolean isClaseGrupal) {
		this.isClaseGrupal = isClaseGrupal;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public ArrayList<Tema> getTemas() {
		return  (ArrayList<Tema>) temas;
	}

	public void setTemas(ArrayList<Tema> temas) {
		this.temas = temas;
	}

	public ArrayList<Alumno> getAlumnos() {
		return (ArrayList<Alumno>) alumnos;
	}

	public void setAlumnos(ArrayList<Alumno> alumnos) {
		this.alumnos = alumnos;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	@Override
	public boolean equals(Object obj) 
	{
		Clase clase = (Clase) obj;
		return clase.getFecha().equals(this.fecha);
	}

	@Override
	public String toString() 
	{
		String temas = "";
		String alumnos = "";
		
		for (Tema tema : this.temas) 
		{
			temas += "\n   -- " + tema.getNombre();
		}
		
		for (Alumno alumno : this.alumnos) 
		{
			alumnos += "\n   -- " + alumno.getNombre();
		}
		
		String claseGrupal = "";
		if(isClaseGrupal)
			claseGrupal = "si";
		else
			claseGrupal = "no";
		
		return "ID: " + id + "\nFECHA: " + fecha + "\nCLASE GRUPAL: "
				+ claseGrupal + "\nDESCRIPCION: " + descripcion + "\nTEMAS: "
				+ temas + "\nALUMNOS: " + alumnos;
	}
}
