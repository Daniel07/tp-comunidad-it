package cliente.domainModel;

import java.io.Serializable;

public class Tema implements Serializable
{
	private static final long serialVersionUID = 1L;
	private long id;
	private String nombre;
	private String descripcion;
	
	private static int incrementalId;
	
	
	public Tema ()
	{
	}
	
	public Tema(long id, String nombre, String descripcion) 
	{
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.id = id;
	}
	
	public Tema(String nombre, String descripcion) 
	{
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		Tema tema = (Tema) obj;
		return tema.getNombre().equals(this.nombre);
	}

	@Override
	public String toString() 
	{
		return "\nNOMBRE: " + nombre + "\nDESCRIPCION: "+ descripcion;
	}
}
