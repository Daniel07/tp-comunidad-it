package usuario.vistas;

import usuario.vistas.controladores.Controlador;

public interface Ventana 
{
	public void mostrarVentana();
	public void ocultarVentana();
	public void limpiarVentana();
	public void actualizarVentana();
	public void asignarControlador(Controlador controlador);
	public String getIdentificadorVentana();
}
