package usuario.vistas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.beans.PropertyVetoException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import usuario.vistas.controladores.Controlador;
import cliente.domainModel.NivelDeEstudio;

@SuppressWarnings("serial")
public class VentanaAdministracionAlumno extends JInternalFrame implements Ventana
{
	private JButton nuevo, aceptar, cancelar, salir;
	private JPanel panelForm, panelTabla;
	private JTextField nombre, telefono, mail, institucionEducativa, ciudadRecidencia;
	private JTextArea descripcion;
	private JComboBox<NivelDeEstudio> nivelDeEstudio; 
	private JToolBar toolBar;
	
	private JTable tablaAlumnos;
	private DefaultTableModel modelAlumnos;
	private String [] columnas = new String[] {"","Nombre", "Telefono", "Mail", "Nivel de estudio", "Intitucion educativa", "Ciudad de recidencia", "Descripcion"};
	
	private String identificador;
	private Controlador controlador;

	public VentanaAdministracionAlumno() 
	{
		this.identificador = "Administracion de alumnos";
		crearVentana();
		crearPanelForm();
		crearPanelMenu();
		crearPanelTabla();
		ocultarPanelForm(true);
	}
	
	private void crearVentana() 
	{
		setBounds(100, 100, 547, 453);
	}

	private void crearPanelForm() 
	{
		panelForm = new JPanel();
		panelForm.setBorder(new LineBorder(new Color(0, 0, 139), 2, true));
		getContentPane().add(panelForm, BorderLayout.EAST);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelForm.setLayout(gbl_panel_1);
		
		crearLabels();
		crearTextFieldForm();
		crearComboBoxForm();
		crearBotonesForm();
	}

	private void crearBotonesForm() 
	{
		aceptar = new JButton("Aceptar");
		GridBagConstraints gbc_btnAceptar = new GridBagConstraints();
		gbc_btnAceptar.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAceptar.insets = new Insets(0, 0, 0, 5);
		gbc_btnAceptar.anchor = GridBagConstraints.NORTH;
		gbc_btnAceptar.gridx = 1;
		gbc_btnAceptar.gridy = 15;
		panelForm.add(aceptar, gbc_btnAceptar);
		
		cancelar = new JButton("Cancelar");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 3;
		gbc_btnNewButton.gridy = 15;
		panelForm.add(cancelar, gbc_btnNewButton);
	}

	private void crearComboBoxForm() 
	{
		nivelDeEstudio = new JComboBox<NivelDeEstudio>();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.gridwidth = 3;
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 8;
		panelForm.add(nivelDeEstudio, gbc_comboBox);
		nivelDeEstudio.addItem(NivelDeEstudio.secundario);
		nivelDeEstudio.addItem(NivelDeEstudio.terciario);
		nivelDeEstudio.addItem(NivelDeEstudio.universitario);
	}

	private void crearTextFieldForm() 
	{
		nombre = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridwidth = 3;
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 2;
		panelForm.add(nombre, gbc_textField);
		nombre.setColumns(10);
		
		telefono = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.gridwidth = 3;
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.insets = new Insets(0, 0, 5, 5);
		gbc_textField_1.gridx = 1;
		gbc_textField_1.gridy = 4;
		panelForm.add(telefono, gbc_textField_1);
		telefono.setColumns(10);
		
		mail = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.gridwidth = 3;
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.insets = new Insets(0, 0, 5, 5);
		gbc_textField_2.gridx = 1;
		gbc_textField_2.gridy = 6;
		panelForm.add(mail, gbc_textField_2);
		mail.setColumns(10);
		
		institucionEducativa = new JTextField();
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.gridwidth = 3;
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.insets = new Insets(0, 0, 5, 5);
		gbc_textField_3.gridx = 1;
		gbc_textField_3.gridy = 10;
		panelForm.add(institucionEducativa, gbc_textField_3);
		institucionEducativa.setColumns(10);
		
		ciudadRecidencia = new JTextField();
		GridBagConstraints gbc_textField_4 = new GridBagConstraints();
		gbc_textField_4.gridwidth = 3;
		gbc_textField_4.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_4.insets = new Insets(0, 0, 5, 5);
		gbc_textField_4.gridx = 1;
		gbc_textField_4.gridy = 12;
		panelForm.add(ciudadRecidencia, gbc_textField_4);
		ciudadRecidencia.setColumns(10);
		
		descripcion = new JTextArea();
		descripcion.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		descripcion.setLineWrap(true);
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.gridwidth = 3;
		gbc_textArea.insets = new Insets(0, 0, 5, 5);
		gbc_textArea.fill = GridBagConstraints.BOTH;
		gbc_textArea.gridx = 1;
		gbc_textArea.gridy = 14;
		panelForm.add(descripcion, gbc_textArea);
		
	}

	private void crearLabels() 
	{
		JLabel lblNombre = new JLabel("Nombre:");
		GridBagConstraints gbc_lblNombre = new GridBagConstraints();
		gbc_lblNombre.gridwidth = 3;
		gbc_lblNombre.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNombre.insets = new Insets(0, 0, 5, 5);
		gbc_lblNombre.gridx = 1;
		gbc_lblNombre.gridy = 1;
		panelForm.add(lblNombre, gbc_lblNombre);
		
		JLabel lblNewLabel = new JLabel("Telefono:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.gridwidth = 3;
		gbc_lblNewLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 1;
		gbc_lblNewLabel.gridy = 3;
		panelForm.add(lblNewLabel, gbc_lblNewLabel);
		
		JLabel lblMail = new JLabel("Mail:");
		GridBagConstraints gbc_lblMail = new GridBagConstraints();
		gbc_lblMail.gridwidth = 5;
		gbc_lblMail.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblMail.insets = new Insets(0, 0, 5, 0);
		gbc_lblMail.gridx = 1;
		gbc_lblMail.gridy = 5;
		panelForm.add(lblMail, gbc_lblMail);
		
		JLabel lblNivelDeEstudio = new JLabel("Nivel de estudio:");
		GridBagConstraints gbc_lblNivelDeEstudio = new GridBagConstraints();
		gbc_lblNivelDeEstudio.gridwidth = 3;
		gbc_lblNivelDeEstudio.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblNivelDeEstudio.insets = new Insets(0, 0, 5, 5);
		gbc_lblNivelDeEstudio.gridx = 1;
		gbc_lblNivelDeEstudio.gridy = 7;
		panelForm.add(lblNivelDeEstudio, gbc_lblNivelDeEstudio);
		
		JLabel lblInstitucinEducativa = new JLabel("Instituci\u00F3n educativa:");
		GridBagConstraints gbc_lblInstitucinEducativa = new GridBagConstraints();
		gbc_lblInstitucinEducativa.gridwidth = 3;
		gbc_lblInstitucinEducativa.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblInstitucinEducativa.insets = new Insets(0, 0, 5, 5);
		gbc_lblInstitucinEducativa.gridx = 1;
		gbc_lblInstitucinEducativa.gridy = 9;
		panelForm.add(lblInstitucinEducativa, gbc_lblInstitucinEducativa);
		
		JLabel lblCiudadDeRecidencia = new JLabel("Ciudad de recidencia:");
		GridBagConstraints gbc_lblCiudadDeRecidencia = new GridBagConstraints();
		gbc_lblCiudadDeRecidencia.gridwidth = 3;
		gbc_lblCiudadDeRecidencia.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblCiudadDeRecidencia.insets = new Insets(0, 0, 5, 5);
		gbc_lblCiudadDeRecidencia.gridx = 1;
		gbc_lblCiudadDeRecidencia.gridy = 11;
		panelForm.add(lblCiudadDeRecidencia, gbc_lblCiudadDeRecidencia);
		
		JLabel lblDescripcin = new JLabel("Descripci\u00F3n:");
		GridBagConstraints gbc_lblDescripcin = new GridBagConstraints();
		gbc_lblDescripcin.gridwidth = 3;
		gbc_lblDescripcin.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblDescripcin.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescripcin.gridx = 1;
		gbc_lblDescripcin.gridy = 13;
		panelForm.add(lblDescripcin, gbc_lblDescripcin);
	}

	private void crearPanelMenu() 
	{
		toolBar = new JToolBar();
		toolBar.setFloatable(false);
		getContentPane().add(toolBar, BorderLayout.NORTH);
		
		crearBotonesMenu();
	}

	private void crearBotonesMenu() 
	{
		nuevo = new JButton("Nuevo");
		toolBar.add(nuevo);
		salir = new JButton("Salir");
		toolBar.add(salir);
	}

	private void crearPanelTabla() 
	{
		panelTabla = new JPanel();
		panelTabla.setBorder(new LineBorder(new Color(0, 0, 139), 2, true));
		getContentPane().add(panelTabla, BorderLayout.CENTER);
		
		crearTabla();
	}
	
	private void crearTabla() 
	{
		panelTabla.setLayout(new BorderLayout(0, 0));
		JScrollPane scrollPane1 = new JScrollPane();
		panelTabla.add(scrollPane1);
		tablaAlumnos = new JTable();
		tablaAlumnos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		modelAlumnos = new DefaultTableModel(null, columnas) {
			@Override
			public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
		};;
		tablaAlumnos.setModel(modelAlumnos);
		scrollPane1.setViewportView(tablaAlumnos);
	}

	public void ocultarPanelForm(boolean b)
	{
		panelForm.setVisible(!b);
		limpiarVentana();
	}
	
	public void ocultarBotonNuevo(boolean b)
	{
		nuevo.setVisible(!b);
	}
	
	public void modoAdministrador(boolean b)
	{
		ocultarPanelForm(!b);
		ocultarBotonNuevo(!b);
	}

	@Override
	public void mostrarVentana() 
	{
		try 
		{
			setMaximum(true);
		}
		catch (PropertyVetoException e) 
		{
			e.printStackTrace();
		}
		setVisible(true);
		limpiarVentana();
	}

	@Override
	public void ocultarVentana() 
	{
		setVisible(false);
	}

	@Override
	public void limpiarVentana() 
	{
		nombre.setText("");
		telefono.setText("");
		mail.setText("");
		institucionEducativa.setText("");
		ciudadRecidencia.setText("");
		descripcion.setText("");
		nivelDeEstudio.setSelectedIndex(0);
	}
	
	@Override
	public void actualizarVentana() 
	{
		limpiarVentana();
		controlador.actualizarVista();
	}

	@Override
	public void asignarControlador(Controlador controlador) 
	{
		this.controlador = controlador;
		nuevo.addActionListener(this.controlador);
		aceptar.addActionListener(this.controlador);
		cancelar.addActionListener(this.controlador);
		salir.addActionListener(this.controlador);
	}

	@Override
	public String getIdentificadorVentana() 
	{
		return identificador;
	}

	public JButton getNuevo() {
		return nuevo;
	}

	public void setNuevo(JButton nuevo) {
		this.nuevo = nuevo;
	}

	public JButton getAceptar() {
		return aceptar;
	}

	public void setAceptar(JButton aceptar) {
		this.aceptar = aceptar;
	}

	public JButton getCancelar() {
		return cancelar;
	}

	public void setCancelar(JButton cancelar) {
		this.cancelar = cancelar;
	}

	public JButton getSalir() {
		return salir;
	}

	public void setSalir(JButton salir) {
		this.salir = salir;
	}

	public JTextField getNombre() {
		return nombre;
	}

	public void setNombre(JTextField nombre) {
		this.nombre = nombre;
	}

	public JTextField getTelefono() {
		return telefono;
	}

	public void setTelefono(JTextField telefono) {
		this.telefono = telefono;
	}

	public JTextField getMail() {
		return mail;
	}

	public void setMail(JTextField mail) {
		this.mail = mail;
	}

	public JTextField getInstitucionEducativa() {
		return institucionEducativa;
	}

	public void setInstitucionEducativa(JTextField institucionEducativa) {
		this.institucionEducativa = institucionEducativa;
	}

	public JTextField getCiudadRecidencia() {
		return ciudadRecidencia;
	}

	public void setCiudadRecidencia(JTextField ciudadRecidencia) {
		this.ciudadRecidencia = ciudadRecidencia;
	}

	public JTextArea getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(JTextArea descripcion) {
		this.descripcion = descripcion;
	}

	public JComboBox<NivelDeEstudio> getNivelDeEstudio() {
		return nivelDeEstudio;
	}

	public void setNivelDeEstudio(JComboBox<NivelDeEstudio> nivelDeEstudio) {
		this.nivelDeEstudio = nivelDeEstudio;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public JPanel getPanelTabla() {
		return panelTabla;
	}

	public JTable getTablaAlumnos() {
		return tablaAlumnos;
	}

	public DefaultTableModel getModelAlumnos() {
		return modelAlumnos;
	}

	public String[] getColumnas() {
		return columnas;
	}
	
}
