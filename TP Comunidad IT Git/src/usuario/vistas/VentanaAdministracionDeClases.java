package usuario.vistas;

import javax.swing.JInternalFrame;
import usuario.vistas.controladores.Controlador;
import javax.swing.JToolBar;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import java.beans.PropertyVetoException;
import javax.swing.table.DefaultTableModel;

public class VentanaAdministracionDeClases extends JInternalFrame implements Ventana
{
	private static final long serialVersionUID = 1L;
	
	private JButton nuevo, salir, verDetalle;
	private JTable tablaClases;
	private DefaultTableModel modelClases;
	private String [] columnas = new String[] {"", "Descripcion", "Fecha"};
	
	private String identificador;
	private Controlador controlador;
	
	public VentanaAdministracionDeClases() 
	{
		identificador = "Administracion de clases";
		crearVentana();
		crearToolBar();
		crearTabla();
	}
	
	private void crearVentana() 
	{
		setBounds(100, 100, 547, 453);
	}
	
	private void crearToolBar()
	{
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		getContentPane().add(toolBar, BorderLayout.NORTH);
		
		nuevo = new JButton("Nuevo");
		toolBar.add(nuevo);
		
		verDetalle = new JButton("Ver detalle");
		toolBar.add(verDetalle);
		
		salir = new JButton("Salir");
		toolBar.add(salir);
	}
	
	@SuppressWarnings("serial")
	private void crearTabla()
	{
		JScrollPane scrollPane1 = new JScrollPane();
		getContentPane().add(scrollPane1);
		tablaClases = new JTable();
		tablaClases.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		modelClases = new DefaultTableModel(null, columnas) {
			@Override
			public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
		};;
		tablaClases.setModel(modelClases);
		scrollPane1.setViewportView(tablaClases);
	}
	

	@Override
	public void mostrarVentana() 
	{
		try 
		{
			setMaximum(true);
		}
		catch (PropertyVetoException e) 
		{
			e.printStackTrace();
		}
		setVisible(true);
		limpiarVentana();
	}
	
	@Override
	public void ocultarVentana() 
	{
		setVisible(false);
	}

	@Override
	public void limpiarVentana() 
	{
	}

	@Override
	public void actualizarVentana() 
	{
		limpiarVentana();
		controlador.actualizarVista();
	}

	@Override
	public void asignarControlador(Controlador controlador) 
	{
		this.controlador = controlador;
		
		nuevo.addActionListener(controlador);
		verDetalle.addActionListener(controlador);
		salir.addActionListener(controlador);
	}

	@Override
	public String getIdentificadorVentana() 
	{
		return identificador;
	}

	public JButton getNuevo() {
		return nuevo;
	}
	
	public JButton getVerDetalle() {
		return verDetalle;
	}

	public JButton getSalir() {
		return salir;
	}

	public JTable getTablaClases() {
		return tablaClases;
	}

	public DefaultTableModel getModelClases() {
		return modelClases;
	}

	public String[] getColumnas() {
		return columnas;
	}

	public String getIdentificador() {
		return identificador;
	}
}
