package usuario.vistas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class VentanaPrincipal extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu menuAdministracion;
	private JMenuItem menuItemAlumno, menuItemTema, menuItemClase;
	private JDesktopPane desktopPane;
	
	@SuppressWarnings("deprecation")
	public VentanaPrincipal() 
	{
		configurarLookAndFeel();
		crearVentana();
		crearContentPane();
		crearMenuBar();
		crearDesktopPane();
		show();
	}

	private void configurarLookAndFeel()
	{
		try {
            UIManager.setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
	}

	private void crearVentana() 
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 568, 410);
		setExtendedState(MAXIMIZED_BOTH);
	}
	
	private void crearContentPane() 
	{
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

	private void crearMenuBar() 
	{
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		menuAdministracion = new JMenu("Administrar");
		menuAdministracion.setHorizontalAlignment(SwingConstants.CENTER);
		menuBar.add(menuAdministracion);
		
		menuItemAlumno = new JMenuItem("Alumnos");
		menuAdministracion.add(menuItemAlumno);
		
		menuItemTema = new JMenuItem("Temas");
		menuAdministracion.add(menuItemTema);
		
		menuItemClase = new JMenuItem("Clases");
		menuAdministracion.add(menuItemClase);
	}

	private void crearDesktopPane() 
	{
		desktopPane = new JDesktopPane();
		desktopPane.setBorder(new LineBorder(new Color(0, 0, 139), 3, true));
		contentPane.add(desktopPane, BorderLayout.CENTER);
	}
	
	public void agregarVentanasInternas(Ventana v)
	{
		desktopPane.add((Component) v);
	}
	
	public void asignarControlador(ActionListener controlador)
	{
		menuItemAlumno.addActionListener(controlador);
		menuItemTema.addActionListener(controlador);
		menuItemClase.addActionListener(controlador);
	}

	public JMenuItem getMenuItemAlumno() {
		return menuItemAlumno;
	}

	public void setMenuItemAlumno(JMenuItem menuItemAlumno) {
		this.menuItemAlumno = menuItemAlumno;
	}

	public JMenuItem getMenuItemTema() {
		return menuItemTema;
	}

	public void setMenuItemTema(JMenuItem menuItemTema) {
		this.menuItemTema = menuItemTema;
	}

	public JMenuItem getMenuItemClase() {
		return menuItemClase;
	}

	public void setMenuItemClase(JMenuItem menuItemClase) {
		this.menuItemClase = menuItemClase;
	}

	public JDesktopPane getDesktopPane() {
		return desktopPane;
	}

	public void setDesktopPane(JDesktopPane desktopPane) {
		this.desktopPane = desktopPane;
	}
	
}
