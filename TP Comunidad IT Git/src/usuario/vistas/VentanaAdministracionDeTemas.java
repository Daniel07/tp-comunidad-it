package usuario.vistas;

import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import usuario.vistas.controladores.Controlador;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;

public class VentanaAdministracionDeTemas extends JInternalFrame implements Ventana
{
	private static final long serialVersionUID = 1L;
	
	private JTextField nombre;
	private JTextArea descripcion;
	private JButton crear, salir;
	
	private JTable tablaTemas;
	private DefaultTableModel modelTemas;
	private String [] columnas = new String[] {"", "Nombre","Descripcion"};
	
	private String identificador;
	private Controlador controlador;
	private JPanel panelTabla;
	
	public VentanaAdministracionDeTemas() 
	{
		identificador = "Administracion de temas";
		crearVentana();
		crearForm();
		crearBotones();
		crearTablaDeTemas();
	}

	private void crearVentana()
	{
		setBounds(100, 100, 621, 302);
		getContentPane().setLayout(null);
	}

	private void crearForm() 
	{
		JLabel lblTema = new JLabel("Tema:");
		lblTema.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTema.setBounds(10, 11, 147, 28);
		getContentPane().add(lblTema);
		
		nombre = new JTextField();
		nombre.setBounds(10, 50, 147, 28);
		getContentPane().add(nombre);
		nombre.setColumns(10);
		
		JLabel lblDescripcin = new JLabel("Descripci\u00F3n:");
		lblDescripcin.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDescripcin.setBounds(10, 89, 147, 28);
		getContentPane().add(lblDescripcin);
		
		descripcion = new JTextArea();
		descripcion.setBorder(new LineBorder(Color.BLACK, 1, true));
		descripcion.setBounds(10, 128, 147, 103);
		getContentPane().add(descripcion);
	}

	private void crearBotones() 
	{
		crear = new JButton("Crear");
		crear.setBounds(10, 242, 147, 23);
		getContentPane().add(crear);
		
		salir = new JButton("Salir");
		salir.setBounds(505, 242, 90, 23);
		getContentPane().add(salir);
	}

	@SuppressWarnings({"serial" })
	private void crearTablaDeTemas() 
	{
		JLabel lblTemas = new JLabel("Temas");
		lblTemas.setBounds(167, 15, 344, 20);
		getContentPane().add(lblTemas);
		lblTemas.setBackground(new Color(100, 149, 237));
		lblTemas.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		panelTabla = new JPanel();
		panelTabla.setBounds(167, 48, 428, 183);
		getContentPane().add(panelTabla);
		panelTabla.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panelTabla.add(scrollPane);
		
		tablaTemas = new JTable();
		tablaTemas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		modelTemas = new DefaultTableModel(null, columnas) {
			@Override
			public boolean isCellEditable(int rowIndex,int columnIndex){return false;}
		};;
		tablaTemas.setModel(modelTemas);
		scrollPane.setViewportView(tablaTemas);
	}

	@Override
	public void mostrarVentana() 
	{
		setVisible(true);
	}

	@Override
	public void ocultarVentana() 
	{
		setVisible(false);
	}

	@Override
	public void limpiarVentana() 
	{
		nombre.setText("");
		descripcion.setText("");
	}

	@Override
	public void actualizarVentana() 
	{
		limpiarVentana();
		controlador.actualizarVista();
	}

	@Override
	public void asignarControlador(Controlador controlador) 
	{
		this.controlador = controlador;
		crear.addActionListener(this.controlador);
		salir.addActionListener(this.controlador);
	}

	@Override
	public String getIdentificadorVentana() 
	{
		return identificador;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public JTextField getNombre() {
		return nombre;
	}

	public JTextArea getDescripcion() {
		return descripcion;
	}

	public JButton getCrear() {
		return crear;
	}

	public JButton getSalir() {
		return salir;
	}

	public JTable getTablaDeTemas() {
		return tablaTemas;
	}

	public DefaultTableModel getModelTemas() {
		return modelTemas;
	}

	public String[] getColumnas() {
		return columnas;
	}
}
