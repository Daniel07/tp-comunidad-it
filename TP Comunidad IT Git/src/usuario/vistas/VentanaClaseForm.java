package usuario.vistas;

import javax.swing.JInternalFrame;

import usuario.vistas.controladores.Controlador;

import javax.swing.JLabel;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

@SuppressWarnings("serial")
public class VentanaClaseForm extends JInternalFrame implements Ventana
{

	private JPanel panelTablaAlumnos, panelTablaTemas;
	
	private JTable tablaAlumnos;
	private DefaultTableModel modelAlumnos;
	
	private JTable tablaTemas;
	private DefaultTableModel modelTemas;
	
	private String [] columnas = new String[] {"", "Nombre","Descripcion", "Seleccionar"};
	
	private JTextArea descripcion;
	private JButton btnAceptar, btnCancelar;
	
	private String identificador;
	private Controlador controlador;
	
	public VentanaClaseForm() 
	{
		identificador = "Form clase";
		crearVentana();
		crearPanelesParaTablas();
		crearTablaAlumnos();
		crearTablaTemas();
		crearTextAreaDescripcion();
		crearBotones();
	}
	
	private void crearVentana() 
	{
		setBounds(100, 100, 642, 382);
		getContentPane().setLayout(null);
	}

	private void crearPanelesParaTablas() 
	{
		JLabel lblAlumnos = new JLabel("Alumnos:");
		lblAlumnos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAlumnos.setBounds(10, 11, 138, 20);
		getContentPane().add(lblAlumnos);
		
		panelTablaAlumnos = new JPanel();
		panelTablaAlumnos.setBounds(10, 42, 300, 174);
		getContentPane().add(panelTablaAlumnos);
		
		JLabel lblTemas = new JLabel("Temas:");
		lblTemas.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTemas.setBounds(320, 16, 138, 20);
		getContentPane().add(lblTemas);
		
		panelTablaTemas = new JPanel();
		panelTablaTemas.setBounds(320, 42, 300, 174);
		getContentPane().add(panelTablaTemas);
	}
	
	private void crearTablaAlumnos() 
	{
		panelTablaAlumnos.setLayout(new BorderLayout(0, 0));
		JScrollPane scrollPane1 = new JScrollPane();
		panelTablaAlumnos.add(scrollPane1);
		tablaAlumnos = new JTable();
		tablaAlumnos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		modelAlumnos = new DefaultTableModel(null, columnas) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
					Object.class, Object.class, Object.class, Boolean.class
				};
				@SuppressWarnings({ "unchecked", "rawtypes" })
				@Override
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false, true
				};
				@Override
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
		};
		tablaAlumnos.setModel(modelAlumnos);
		scrollPane1.setViewportView(tablaAlumnos);
	}

	private void crearTablaTemas() 
	{
		panelTablaTemas.setLayout(new BorderLayout(0, 0));
		JScrollPane scrollPane1 = new JScrollPane();
		panelTablaTemas.add(scrollPane1);
		tablaTemas = new JTable();
		tablaTemas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		modelTemas = new DefaultTableModel(null, columnas) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
					Object.class, Object.class, Object.class, Boolean.class
				};
				@SuppressWarnings({ "unchecked", "rawtypes" })
				@Override
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false, true
				};
				@Override
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
		};
		tablaTemas.setModel(modelTemas);
		scrollPane1.setViewportView(tablaTemas);
	}

	private void crearTextAreaDescripcion() 
	{
		JLabel lblDescripcion = new JLabel("Descripcion:");
		lblDescripcion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDescripcion.setBounds(10, 226, 138, 20);
		getContentPane().add(lblDescripcion);
		
		descripcion = new JTextArea();
		descripcion.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		descripcion.setBounds(10, 257, 607, 49);
		getContentPane().add(descripcion);
	}

	private void crearBotones() 
	{
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(10, 317, 89, 23);
		getContentPane().add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(528, 317, 89, 23);
		getContentPane().add(btnCancelar);
	}

	@Override
	public void mostrarVentana() 
	{
		show();
		setVisible(true);
		actualizarVentana();
	}

	@Override
	public void ocultarVentana() 
	{
		setVisible(false);
	}

	@Override
	public void limpiarVentana() 
	{
		descripcion.setText("");
	}

	@Override
	public void actualizarVentana() 
	{
		limpiarVentana();
		controlador.actualizarVista();
	}

	@Override
	public void asignarControlador(Controlador controlador) 
	{
		this.controlador = controlador;
		btnAceptar.addActionListener(controlador);
		btnCancelar.addActionListener(controlador);
	}

	@Override
	public String getIdentificadorVentana() 
	{
		return identificador;
	}

	public JTable getTablaAlumnos() {
		return tablaAlumnos;
	}

	public DefaultTableModel getModelAlumnos() {
		return modelAlumnos;
	}

	public JTable getTablaTemas() {
		return tablaTemas;
	}

	public DefaultTableModel getModelTemas() {
		return modelTemas;
	}

	public String[] getColumnas() {
		return columnas;
	}

	public JTextArea getDescripcion() {
		return descripcion;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}
}
