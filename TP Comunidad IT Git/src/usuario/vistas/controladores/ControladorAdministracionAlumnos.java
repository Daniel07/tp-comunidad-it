package usuario.vistas.controladores;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import cliente.Servicios.AlumnoService;
import cliente.domainModel.Alumno;
import cliente.domainModel.NivelDeEstudio;
import usuario.vistas.VentanaAdministracionAlumno;

public class ControladorAdministracionAlumnos implements Controlador
{

	private VentanaAdministracionAlumno ventana;
	@SuppressWarnings("unused")
	private Navegador navegador;
	private AlumnoService service;
	
	public ControladorAdministracionAlumnos(VentanaAdministracionAlumno v, Navegador n, AlumnoService s) 
	{
		navegador = n;
		ventana = v;
		ventana.asignarControlador(this);
		service = s;
		actualizarTabla();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == ventana.getNuevo())
		{
			ventana.ocultarPanelForm(false);
		}
		
		if(e.getSource() == ventana.getAceptar())
		{
			registrarAlumno();
			ventana.ocultarPanelForm(true);
			actualizarTabla();
		}
		
		if(e.getSource() == ventana.getCancelar())
		{
			ventana.ocultarPanelForm(true);
		}
		
		if(e.getSource() == ventana.getSalir())
		{
			ventana.ocultarVentana();
			ventana.ocultarPanelForm(true);
		}
		
	}
	
	private void registrarAlumno() 
	{
		String nombre = ventana.getNombre().getText();
		String tel = ventana.getTelefono().getText();
		String mail = ventana.getMail().getText();
		NivelDeEstudio nivel = (NivelDeEstudio) ventana.getNivelDeEstudio().getSelectedItem();
		String institucion = ventana.getInstitucionEducativa().getText();
		String ciudad = ventana.getCiudadRecidencia().getText();
		String descripcion = ventana.getDescripcion().getText();
		
		service.crearAlumno(nombre, tel, mail, nivel, institucion, ciudad, descripcion);
	}

	public void actualizarTabla()
	{
		limpiarTabla();
		ArrayList<Alumno> alumnos = (ArrayList<Alumno>) service.getAlumnos();
		for (int i = 0; i < alumnos.size(); i++ ) 
		{
			Alumno a = alumnos.get(i);
			Object[] fila = {i, a.getNombre(), a.getTelefono(), a.getMail(), a.getNivel(), a.getInstitucionEducativa(), a.getCiudadDeRecidencia(), a.getDescripcion()};
			ventana.getModelAlumnos().addRow(fila);
		}
	}
	
	private void limpiarTabla() 
	{
		ventana.getModelAlumnos().setRowCount(0); //Para vaciar la tabla
		ventana.getModelAlumnos().setColumnCount(0);
		ventana.getModelAlumnos().setColumnIdentifiers(ventana.getColumnas());
		ocultarColumnaDePosicionEnTabla();
	}
	
	private void ocultarColumnaDePosicionEnTabla() 
	{
		ventana.getTablaAlumnos().getColumnModel().getColumn(0).setPreferredWidth(0);
		ventana.getTablaAlumnos().getColumnModel().getColumn(0).setMinWidth(0);
		ventana.getTablaAlumnos().getColumnModel().getColumn(0).setMaxWidth(0);
	}

	@Override
	public void actualizarVista() 
	{
		actualizarTabla();
	}
	
}
