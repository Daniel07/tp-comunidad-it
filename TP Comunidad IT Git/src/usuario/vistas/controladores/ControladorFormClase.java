package usuario.vistas.controladores;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import cliente.Servicios.AlumnoService;
import cliente.Servicios.ClaseService;
import cliente.Servicios.TemaService;
import cliente.domainModel.Alumno;
import cliente.domainModel.Tema;
import usuario.vistas.VentanaClaseForm;

public class ControladorFormClase implements Controlador
{
	private VentanaClaseForm ventana;
	private Navegador navegador;
	private ClaseService claseService;
	private AlumnoService alumnoService;
	private TemaService temaService;
	
	private ArrayList<Alumno> alumnos;
	private ArrayList<Tema> temas;
	
	public ControladorFormClase(VentanaClaseForm v, Navegador n, ClaseService claseService, AlumnoService alumnoService, TemaService temaService) 
	{
		ventana = v;
		navegador = n;
		this.claseService = claseService;
		this.alumnoService = alumnoService;
		this.temaService = temaService;
		
		alumnos = new ArrayList<Alumno>();
		temas = new ArrayList<Tema>();
		
		ventana.asignarControlador(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == ventana.getBtnAceptar())
		{
			crearClase();
			ventana.ocultarVentana();
			navegador.actualizarVentana("Administracion de clases");
		}
		
		if(e.getSource() == ventana.getBtnCancelar())
		{
			ventana.ocultarVentana();
		}
		
	}

	private void crearClase() 
	{
		ArrayList<Alumno> alumnosSeleccionados = obtenerAlumnosSeleccionados();
		ArrayList<Tema> temasSeleccionados = obtenerTemasSeleccionados();
		boolean esGrupal = 1 < alumnosSeleccionados.size();
		String descripcion = ventana.getDescripcion().getText();
		claseService.crearClase(esGrupal, descripcion, temasSeleccionados, alumnosSeleccionados);
	}
	
	private ArrayList<Alumno> obtenerAlumnosSeleccionados()
	{
		ArrayList<Alumno> seleccionados = new ArrayList<Alumno>();
		for (int i = 0; i < alumnos.size(); i++) 
		{
			boolean seleccionado = (boolean) ventana.getTablaAlumnos().getValueAt(i, 3);
			if(seleccionado)
			{
				seleccionados.add(alumnos.get(i));
			}
		}
		return seleccionados;
	}
	
	private ArrayList<Tema> obtenerTemasSeleccionados()
	{
		ArrayList<Tema> seleccionados = new ArrayList<Tema>();
		for (int i = 0; i < temas.size(); i++) 
		{
			boolean seleccionado = (boolean) ventana.getTablaTemas().getValueAt(i, 3);
			if(seleccionado)
			{
				seleccionados.add(temas.get(i));
			}
		}
		return seleccionados;
	}
	
	
	private void actualizarTablas()
	{
		actualizarTablaAlumnos();
		actualizarTablaTemas();
	}
	
	private void actualizarTablaAlumnos()
	{
		limpiarTabla(ventana.getModelAlumnos());
		alumnos = (ArrayList<Alumno>) alumnoService.getAlumnos();
		for (int i = 0; i < alumnos.size(); i++ ) 
		{
			Alumno a = alumnos.get(i);
			Object[] fila = {i, a.getNombre(),a.getDescripcion(),false};
			ventana.getModelAlumnos().addRow(fila);
		}
	} 
	
	private void actualizarTablaTemas()
	{
		limpiarTabla(ventana.getModelTemas());
		temas = temaService.getTemas();
		for (int i = 0; i < temas.size(); i++ ) 
		{
			Tema t = temas.get(i);
			Object[] fila = {i, t.getNombre(),t.getDescripcion(), false};
			ventana.getModelTemas().addRow(fila);
		}
	} 
	
	private void limpiarTabla(DefaultTableModel model) 
	{
		model.setRowCount(0); //Para vaciar la tabla
		model.setColumnCount(0);
		model.setColumnIdentifiers(ventana.getColumnas());
		ocultarColumnaDePosicionEnTabla(ventana.getTablaAlumnos());
		ocultarColumnaDePosicionEnTabla(ventana.getTablaTemas());
	}
	
	private void ocultarColumnaDePosicionEnTabla(JTable tabla) 
	{
		tabla.getColumnModel().getColumn(0).setPreferredWidth(0);
		tabla.getColumnModel().getColumn(0).setMinWidth(0);
		tabla.getColumnModel().getColumn(0).setMaxWidth(0);
	}
	
	@Override
	public void actualizarVista() 
	{
		actualizarTablas();
	}

}
