package usuario.vistas.controladores;

import java.util.HashMap;
import usuario.vistas.Ventana;

public class Navegador 
{
	private HashMap<String, Ventana> contenedor;
	private static Navegador instancia;
	
	private Navegador() 
	{
		contenedor = new HashMap<String, Ventana>();
	}
	
	public void agregarVentana(Ventana v)
	{
		contenedor.put(v.getIdentificadorVentana(), v);
	}
	
	public void ocultarVentana(String identificadorVentanaActual)
	{
		Ventana ventanaActual = contenedor.get(identificadorVentanaActual);
		ventanaActual.ocultarVentana();
	}
	
	public void mostrarVentana(String identificadorVentanaAMostrar)
	{
		Ventana ventanaActual = contenedor.get(identificadorVentanaAMostrar);
		ventanaActual.mostrarVentana();
	}
	
	public void cambiarVentana(String identificadorVentanaActual, String identificadorVentanaSiguiente)
	{
		Ventana ventanaActual = contenedor.get(identificadorVentanaActual);
		Ventana ventanaSiguiente = contenedor.get(identificadorVentanaSiguiente);
		intercambiarVentanas(ventanaActual, ventanaSiguiente);
	}

	private void intercambiarVentanas(Ventana ventanaActual, Ventana ventanaSiguiente) 
	{
		ventanaActual.ocultarVentana();
		ventanaSiguiente.mostrarVentana();
	}
	
	public void actualizarVentana(String identificadorVentana)
	{
		contenedor.get(identificadorVentana).actualizarVentana();
	}
	
	public Ventana getVentana(String identificadorVentana)
	{
		return contenedor.get(identificadorVentana);
	}
	
	public static Navegador getInstanciaNavegador()
	{
		if(instancia == null)
			instancia = new Navegador();
		return instancia;
	}
}
