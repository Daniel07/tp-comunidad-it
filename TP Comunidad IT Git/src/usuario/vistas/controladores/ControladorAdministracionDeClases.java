package usuario.vistas.controladores;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import cliente.Servicios.ClaseService;
import cliente.domainModel.Clase;
import usuario.vistas.VentanaAdministracionDeClases;

public class ControladorAdministracionDeClases implements Controlador
{

	private VentanaAdministracionDeClases ventana;
	private Navegador navegador;
	private ClaseService service;
	private ArrayList<Clase> clases;
	
	public ControladorAdministracionDeClases(VentanaAdministracionDeClases v, Navegador n, ClaseService s) 
	{
		clases = new ArrayList<Clase>();
		ventana = v;
		navegador = n;
		service = s;
		v.asignarControlador(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == ventana.getNuevo())
		{
			navegador.mostrarVentana("Form clase");
		}
		
		if(e.getSource() == ventana.getVerDetalle())
		{
			mostrarDetalle();
		}
		
		if(e.getSource() == ventana.getSalir())
		{
			ventana.ocultarVentana();
		}
	}
	
	private void actualizarTabla()
	{
		limpiarTabla();
		clases = service.getClases();
		for (int i = 0; i < clases.size(); i++ ) 
		{
			Clase c = clases.get(i);
			Object[] fila = {i, c.getDescripcion(), c.getFecha()};
			ventana.getModelClases().addRow(fila);
		}
	} 
	
	private void limpiarTabla() 
	{
		ventana.getModelClases().setRowCount(0); //Para vaciar la tabla
		ventana.getModelClases().setColumnCount(0);
		ventana.getModelClases().setColumnIdentifiers(ventana.getColumnas());
		reconfigurarColumnas();
		
	}
	
	private void reconfigurarColumnas() 
	{
		ocultarColumnaDePosicionEnTabla();
		ventana.getTablaClases().getColumnModel().getColumn(2).setPreferredWidth(200);
		ventana.getTablaClases().getColumnModel().getColumn(2).setMinWidth(200);
		ventana.getTablaClases().getColumnModel().getColumn(2).setMaxWidth(500);
//		ventana.getTablaClases().getColumnModel().getColumn(2).setResizable(false);
//		ventana.getTablaClases().getColumnModel().getColumn(1).setResizable(false);
	}

	private void ocultarColumnaDePosicionEnTabla() 
	{
		ventana.getTablaClases().getColumnModel().getColumn(0).setPreferredWidth(0);
		ventana.getTablaClases().getColumnModel().getColumn(0).setMinWidth(0);
		ventana.getTablaClases().getColumnModel().getColumn(0).setMaxWidth(0);
	}
	
	private void mostrarDetalle()
	{
		int index = ventana.getTablaClases().getSelectedRow();
		if(index >= 0)
		{
			int posicion = (int) ventana.getTablaClases().getValueAt(index, 0);
			Clase claseSeleccionada = clases.get(posicion);
			String detalleCompleto = claseSeleccionada.toString();
			JOptionPane.showMessageDialog(null, detalleCompleto);
		}
		
		else 
		{
			JOptionPane.showMessageDialog(null, "Seleccione una clase para ver su detalle");
		}
	}

	@Override
	public void actualizarVista() 
	{
		actualizarTabla();
	}

}
