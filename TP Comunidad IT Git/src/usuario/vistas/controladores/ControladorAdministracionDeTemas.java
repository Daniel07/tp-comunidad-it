package usuario.vistas.controladores;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import usuario.vistas.VentanaAdministracionDeTemas;
import cliente.Servicios.TemaService;
import cliente.domainModel.Tema;

public class ControladorAdministracionDeTemas implements Controlador
{
	private TemaService service;
	private VentanaAdministracionDeTemas ventana;
	@SuppressWarnings("unused")
	private Navegador navegador;
	
	public ControladorAdministracionDeTemas(VentanaAdministracionDeTemas v, Navegador n, TemaService s) 
	{
		service = s;
		ventana = v;
		navegador = n;
		v.asignarControlador(this);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == ventana.getCrear())
		{
			crearTema();
			ventana.actualizarVentana();
		}
		
		if(e.getSource() == ventana.getSalir())
		{
			ventana.ocultarVentana();
		}
	}

	private void crearTema() 
	{
		String nombre = ventana.getNombre().getText();
		String descripcion = ventana.getDescripcion().getText();
		service.crearTema(nombre, descripcion);
	}
	
	public void actualizarTabla()
	{
		limpiarTabla();
		ArrayList<Tema> temas = (ArrayList<Tema>) service.getTemas();
		for (int i = 0; i < temas.size(); i++ ) 
		{
			Tema t = temas.get(i);
			Object[] fila = {i, t.getNombre(), t.getDescripcion()};
			ventana.getModelTemas().addRow(fila);
		}
	}
	
	private void limpiarTabla() 
	{
		ventana.getModelTemas().setRowCount(0); //Para vaciar la tabla
		ventana.getModelTemas().setColumnCount(0);
		ventana.getModelTemas().setColumnIdentifiers(ventana.getColumnas());
		ocultarColumnaDePosicionEnTabla();
	}
	
	private void ocultarColumnaDePosicionEnTabla() 
	{
		ventana.getTablaDeTemas().getColumnModel().getColumn(0).setPreferredWidth(0);
		ventana.getTablaDeTemas().getColumnModel().getColumn(0).setMinWidth(0);
		ventana.getTablaDeTemas().getColumnModel().getColumn(0).setMaxWidth(0);
	}

	@Override
	public void actualizarVista() 
	{
		actualizarTabla();
	}

}
