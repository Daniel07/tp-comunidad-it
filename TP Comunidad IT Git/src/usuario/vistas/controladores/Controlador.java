package usuario.vistas.controladores;

import java.awt.event.ActionListener;

public interface Controlador extends ActionListener
{
	public void actualizarVista();
}
