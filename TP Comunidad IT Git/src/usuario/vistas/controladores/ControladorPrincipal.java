package usuario.vistas.controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import usuario.vistas.VentanaPrincipal;

public class ControladorPrincipal implements ActionListener
{
	private VentanaPrincipal ventana;
	private Navegador navegador;
	
	public ControladorPrincipal(VentanaPrincipal ventanaPrincipal, Navegador navegador) 
	{
		this.navegador = navegador;
		ventana = ventanaPrincipal;
		ventana.asignarControlador(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == ventana.getMenuItemAlumno())
		{
			navegador.mostrarVentana("Administracion de alumnos");
			navegador.actualizarVentana("Administracion de alumnos");
		}
		
		if(e.getSource() == ventana.getMenuItemTema())
		{
			navegador.mostrarVentana("Administracion de temas");
			navegador.actualizarVentana("Administracion de temas");
		}
		
		if(e.getSource() == ventana.getMenuItemClase())
		{
			navegador.mostrarVentana("Administracion de clases");
			navegador.actualizarVentana("Administracion de clases");
		}
	}
	
}
