package persistencia.DAOJson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import cliente.domainModel.Clase;
import persistencia.DAO.ClaseDAO;

public class ClaseDAOJson implements ClaseDAO
{

	private String archivoJson;
	
	public ClaseDAOJson() 
	{
		this.archivoJson = "Datos/Datos clases.json";
	}
	
	@Override
	public Clase insert(Clase c) 
	{
		ArrayList<Clase> clases = cargarClases();
		clases.add(c);
		guardarClases(clases);
		return c;
	}

	@Override
	public List<Clase> readAll() 
	{
		return cargarClases();
	}

	@Override
	public Clase getClasePorId(int id) 
	{
		ArrayList<Clase> clases = cargarClases();
		for (Clase clase : clases) 
		{
			if(clase.getId() == id)
				return clase;
		}
		return null;
	}
	
	private void guardarClases ( ArrayList <Clase> clases)
	{
		JSONArray listaJsonClases = armarJsonArrayDeClases(clases);
		
		try 
		{
			FileWriter file = new FileWriter(archivoJson);
			file.write(listaJsonClases.toJSONString());
			file.flush();
			file.close();
		} 
		catch (IOException e) 
		{
			System.out.println("problema al guardar");
		}
	}
			
	@SuppressWarnings("unchecked")
	public static JSONArray armarJsonArrayDeClases(ArrayList<Clase> clases) 
	{
		JSONArray listaJsonOf = new JSONArray();
		JSONArray listaJsonClases = new JSONArray();
		for (int i = 0; i < clases.size(); i++) 
		{
			JSONObject objetoClase = armarJsonObjectClase(clases.get(i));
			listaJsonOf.add(objetoClase);
			listaJsonClases.add(listaJsonOf);
		}

		return listaJsonClases;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject armarJsonObjectClase(Clase clase) 
	{
		JSONObject objetoClase = new JSONObject();
		
		objetoClase.put("id:", clase.getId());
		objetoClase.put("fecha:", clase.getFecha().toString());
		objetoClase.put("grupal:", clase.isClaseGrupal());
		objetoClase.put("descripcion:", clase.getDescripcion());
		objetoClase.put("temas en clase:", TemaDAOJson.armarJsonArrayDeTemas(clase.getTemas()));
		objetoClase.put("alumnos en clase:", AlumnoDAOJson.armarJsonArrayDeAlumnos(clase.getAlumnos()));
		return objetoClase;
	}

	private ArrayList <Clase> cargarClases()
	{
		JSONParser parser = new JSONParser();
		ArrayList<Clase> clases = new ArrayList<Clase>();

		try 
		{
			Object obj = parser.parse(new FileReader(archivoJson));
			JSONArray listaJson = (JSONArray) obj;
			clases = armarArrayListDeClases(listaJson);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			System.out.println("problema con el archivo");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			System.out.println("Exepcion");
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
			System.out.println("problema con el parseo");
		}
		
		return clases;
	}

	public static ArrayList<Clase> armarArrayListDeClases(JSONArray listaJson) 
	{
		ArrayList<Clase> clases = new ArrayList<Clase>();
		for (int i = 0; i < listaJson.size(); i++) 
		{
			JSONArray listaJsonLeida = (JSONArray) listaJson.get(0);
			JSONObject claseJson = (JSONObject) listaJsonLeida.get(i);
			Long id = (Long) claseJson.get("id:");
			String fecha = (String) claseJson.get("fecha:");
			boolean isGrupal = (boolean) claseJson.get("grupal:");
			String descripcion = (String) claseJson.get("descripcion:");
			JSONArray temas =  (JSONArray) claseJson.get("temas en clase:");
			JSONArray alumnos = (JSONArray) claseJson.get("alumnos en clase:");
			
			Clase claseJava = new Clase(id, fecha, isGrupal, descripcion);
			claseJava.setTemas(TemaDAOJson.armarArrayListDeTemas(temas));
			claseJava.setAlumnos(AlumnoDAOJson.armarArrayListDeAlumnos(alumnos));
	
			clases.add(claseJava);
		}
		return clases;
	}
	
	
}
