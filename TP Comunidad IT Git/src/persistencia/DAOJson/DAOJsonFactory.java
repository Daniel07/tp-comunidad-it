package persistencia.DAOJson;

import persistencia.DAO.AlumnoDAO;
import persistencia.DAO.ClaseDAO;
import persistencia.DAO.DAOAbstractFactory;
import persistencia.DAO.TemaDAO;

public class DAOJsonFactory implements DAOAbstractFactory
{
	@Override
	public AlumnoDAO createAlumnoDAO() 
	{
		return new AlumnoDAOJson();
	}

	@Override
	public TemaDAO createTemaDAO() 
	{
		return new TemaDAOJson();
	}

	@Override
	public ClaseDAO createClaseDAO() 
	{
		return new ClaseDAOJson();
	}
	
}
