package persistencia.DAOJson;

import java.util.List;
import cliente.domainModel.Alumno;
import persistencia.DAO.AlumnoDAO;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class AlumnoDAOJson implements AlumnoDAO, Serializable
{
	private static final long serialVersionUID = 1L;
	private String archivoJson;
	private ArrayList<Alumno> alumnos = new ArrayList<Alumno>();
	
	public AlumnoDAOJson() 
	{
		this.archivoJson = "Datos/Datos alumnos.json";
		alumnos = cargarAlumnos();
	}

	@Override
	public Alumno insert(Alumno a) 
	{
		
//		ArrayList<Alumno> alumnos = cargarAlumnos();
		alumnos.add(a);
		guardarAlumnos(alumnos);
//		guardarAlumnos(archivoJson);
		return a;
	}

	@Override
	public Alumno getAlumnoPorId(int id) 
	{
		ArrayList<Alumno> alumnos = cargarAlumnos();
		for (Alumno alumno : alumnos) 
		{
			if(alumno.getId() == id)
				return alumno;
		}
		return null;
	}

	@Override
	public List<Alumno> readAll() 
	{
		return cargarAlumnos();
	}
	
	private void guardarAlumnos ( ArrayList <Alumno> alumnos)
	{
		JSONArray listaJsonAlumnos = armarJsonArrayDeAlumnos(alumnos);

		try 
		{
			FileWriter file = new FileWriter(archivoJson);
			file.write(listaJsonAlumnos.toJSONString());
			file.flush();
			file.close();
		} 
		catch (IOException e) 
		{
			System.out.println("problema al guardar");
		}
	}
			
	@SuppressWarnings("unchecked")
	public static JSONArray armarJsonArrayDeAlumnos(ArrayList<Alumno> alumnos) 
	{
//		JSONArray listaJsonOf = new JSONArray();
		JSONArray listaJsonAlumnos = new JSONArray();
		for (int i = 0; i < alumnos.size(); i++) 
		{
			JSONObject objetoAlumno = armarAlumnoJsonObject(alumnos.get(i));
//			listaJsonOf.add(objetoAlumno);
//			listaJsonAlumnos.add(listaJsonOf);
			listaJsonAlumnos.add(objetoAlumno);
		}
		return listaJsonAlumnos;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject armarAlumnoJsonObject(Alumno alumno) 
	{
		JSONObject objetoAlumno = new JSONObject();
		
		objetoAlumno.put("id:", alumno.getId());
		objetoAlumno.put("nombre:", alumno.getNombre());
		objetoAlumno.put("telefono:", alumno.getTelefono());
		objetoAlumno.put("mail:", alumno.getMail());
		objetoAlumno.put("nivel:", alumno.getNivel());
		objetoAlumno.put("institucion educativa:", alumno.getInstitucionEducativa());
		objetoAlumno.put("ciudad de recidencia:", alumno.getCiudadDeRecidencia());
		objetoAlumno.put("descripcion:", alumno.getDescripcion());
		return objetoAlumno;
	}

	private ArrayList <Alumno> cargarAlumnos()
	{
		JSONParser parser = new JSONParser();
		ArrayList<Alumno> alumnosRet = new ArrayList<Alumno>();
		try 
		{
			Object obj = parser.parse(new FileReader(archivoJson));
			JSONArray listaJson = (JSONArray) obj;
			
			alumnosRet = armarArrayListDeAlumnos(listaJson);
			
		} 
		
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			System.out.println("problema con el archivo");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			System.out.println("Exepcion");
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
			System.out.println("problema con el parseo");
		}
		
		return alumnosRet;
	}

	public static ArrayList<Alumno> armarArrayListDeAlumnos(JSONArray listaJson) 
	{
		ArrayList<Alumno> ret = new ArrayList<Alumno>();
		for (int i = 0; i < listaJson.size(); i++) 
		{
			JSONObject alumno = (JSONObject) listaJson.get(i);
			Long id = (Long) alumno.get("id:");
			String nombre = (String) alumno.get("nombre:");
			String telefono = (String) alumno.get("telefono:");
			String mail = (String) alumno.get("mail:");
			String nivel = (String) alumno.get("nivel:");
			String institucionEducativa  = (String) alumno.get("institucion educativa:");
			String ciudadDeRecidencia = (String) alumno.get("ciudad de recidencia:");
			String descripcion = (String) alumno.get("descripcion:");
			
			Alumno alumnoJava = new Alumno(id, nombre, telefono, mail, nivel, institucionEducativa, ciudadDeRecidencia, descripcion);
	
			ret.add(alumnoJava);
		}
		return ret;
	}
	
}
