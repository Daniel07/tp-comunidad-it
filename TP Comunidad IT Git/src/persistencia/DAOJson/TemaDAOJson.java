package persistencia.DAOJson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import cliente.domainModel.Tema;
import persistencia.DAO.TemaDAO;

public class TemaDAOJson implements TemaDAO
{
	private String archivoJson;
	
	public TemaDAOJson() 
	{
		this.archivoJson = "Datos/Datos temas.json";
	}
	
	@Override
	public Tema insert(Tema t) 
	{
		ArrayList<Tema> temas = cargarTemas();
		temas.add(t);
		guardarTemas(temas);
		return t;
	}

	@Override
	public List<Tema> readAll() 
	{
		return cargarTemas();
	}

	@Override
	public Tema getTemaPorId(int id) 
	{
		ArrayList<Tema> temas = cargarTemas();
		for (Tema tema : temas) 
		{
			if(tema.getId() == id)
				return tema;
		}
		return null;
	}
	
	private void guardarTemas ( ArrayList <Tema> temas)
	{
		JSONArray listaJsonTemas = armarJsonArrayDeTemas(temas);

		try 
		{
			FileWriter file = new FileWriter(archivoJson);
			file.write(listaJsonTemas.toJSONString());
			file.flush();
			file.close();
		} 
		catch (IOException e) 
		{
			System.out.println("problema al guardar");
		}
	}
			
	@SuppressWarnings("unchecked")
	public static JSONArray armarJsonArrayDeTemas(ArrayList<Tema> temas) 
	{
		JSONArray listaJsonOf = new JSONArray();
		
		JSONArray listaJsonTemas = new JSONArray();
		for (int i = 0; i < temas.size(); i++) 
		{
			JSONObject objetoTema = armarJsonObjectTema(temas.get(i));
			listaJsonOf.add(objetoTema);
			listaJsonTemas.add(listaJsonOf);
		}
		return listaJsonTemas;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject armarJsonObjectTema(Tema tema) 
	{
		JSONObject objetoTema = new JSONObject();
		objetoTema.put("id:", tema.getId());
		objetoTema.put("nombre:", tema.getNombre());
		objetoTema.put("descripcion:", tema.getDescripcion());
		return objetoTema;
	}

	private ArrayList <Tema> cargarTemas()
	{
		JSONParser parser = new JSONParser();
		ArrayList<Tema> temas = new ArrayList<Tema>();

		try 
		{
			Object obj = parser.parse(new FileReader(archivoJson));
			JSONArray listaJson = (JSONArray) obj;
			temas = armarArrayListDeTemas(listaJson);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			System.out.println("problema con el archivo");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			System.out.println("Exepcion");
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
			System.out.println("problema con el parseo");
		}
		
		return temas;
	}

	public static ArrayList<Tema> armarArrayListDeTemas(JSONArray listaJson) 
	{
		ArrayList<Tema> temas = new ArrayList<Tema>();
		for (int i = 0; i < listaJson.size(); i++) 
		{
			JSONArray listaJsonLeida = (JSONArray) listaJson.get(0);
			JSONObject temaJson = (JSONObject) listaJsonLeida.get(i);
			Long id = (Long) temaJson.get("id:");
			String nombre = (String) temaJson.get("nombre:");
			String descripcion = (String) temaJson.get("descripcion:");
			
			Tema temaJava = new Tema(id, nombre, descripcion);
	
			temas.add(temaJava);
		}
		return temas;
	}
	
}
