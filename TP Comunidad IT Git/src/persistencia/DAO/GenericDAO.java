package persistencia.DAO;

import java.util.List;

public interface GenericDAO<T> 
{
	public T insert(T t);
	public List<T> readAll();
}
