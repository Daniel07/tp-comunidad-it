package persistencia.DAO;


import cliente.domainModel.Alumno;

public interface AlumnoDAO extends GenericDAO<Alumno>
{
	public Alumno getAlumnoPorId(int id);
}
