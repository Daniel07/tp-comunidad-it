package persistencia.DAO;

import cliente.domainModel.Tema;

public interface TemaDAO extends GenericDAO<Tema>
{
	public Tema getTemaPorId(int id);
}
