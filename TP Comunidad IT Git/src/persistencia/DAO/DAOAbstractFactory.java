package persistencia.DAO;


public interface DAOAbstractFactory 
{
	public AlumnoDAO createAlumnoDAO();
	public TemaDAO createTemaDAO();
	public ClaseDAO createClaseDAO();
}
