package persistencia.DAO;

import cliente.domainModel.Clase;

public interface ClaseDAO extends GenericDAO<Clase>
{
	public Clase getClasePorId(int id);
}
