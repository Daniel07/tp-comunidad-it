package Prueba;

import persistencia.DAOJson.DAOJsonFactory;
import cliente.Servicios.AlumnoService;
import cliente.Servicios.ClaseService;
import cliente.Servicios.TemaService;
import usuario.vistas.VentanaAdministracionAlumno;
import usuario.vistas.VentanaAdministracionDeClases;
import usuario.vistas.VentanaAdministracionDeTemas;
import usuario.vistas.VentanaClaseForm;
import usuario.vistas.VentanaPrincipal;
import usuario.vistas.controladores.ControladorAdministracionDeClases;
import usuario.vistas.controladores.ControladorAdministracionDeTemas;
import usuario.vistas.controladores.ControladorFormClase;
import usuario.vistas.controladores.Navegador;
import usuario.vistas.controladores.ControladorAdministracionAlumnos;
import usuario.vistas.controladores.ControladorPrincipal;

public class Main {

	public static void main(String[] args) 
	{
		DAOJsonFactory daoFactory = new DAOJsonFactory();
		AlumnoService alumnoService = new AlumnoService(daoFactory);
		TemaService temaService = new TemaService(daoFactory);
		ClaseService claseService = new ClaseService(daoFactory);
		
		Navegador n = Navegador.getInstanciaNavegador();
		
		VentanaPrincipal v_principal = new VentanaPrincipal();
		VentanaAdministracionAlumno v_administracion_alumnos = new VentanaAdministracionAlumno();
		VentanaAdministracionDeTemas v_administracion_temas = new VentanaAdministracionDeTemas();
		VentanaAdministracionDeClases v_administracion_clases = new VentanaAdministracionDeClases();
		VentanaClaseForm v_clase_form = new VentanaClaseForm();
		
		n.agregarVentana(v_administracion_alumnos);
		n.agregarVentana(v_administracion_temas);
		n.agregarVentana(v_clase_form);
		n.agregarVentana(v_administracion_clases);
		
		v_principal.agregarVentanasInternas(v_administracion_alumnos);
		v_principal.agregarVentanasInternas(v_administracion_temas);
		v_principal.agregarVentanasInternas(v_administracion_clases);
		v_principal.agregarVentanasInternas(v_clase_form);
		
		ControladorPrincipal c_principal = new ControladorPrincipal(v_principal, n);
		ControladorAdministracionAlumnos c_administracion_alumnos = new ControladorAdministracionAlumnos(v_administracion_alumnos, n, alumnoService);
		ControladorAdministracionDeTemas c_administracion_temas = new ControladorAdministracionDeTemas(v_administracion_temas, n, temaService);
		ControladorAdministracionDeClases c_administracion_clases = new ControladorAdministracionDeClases(v_administracion_clases, n, claseService);
		ControladorFormClase c_form_clase = new ControladorFormClase(v_clase_form, n, claseService, alumnoService, temaService);
	}

}
