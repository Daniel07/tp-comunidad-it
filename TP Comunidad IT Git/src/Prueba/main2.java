package Prueba;

import persistencia.DAOJson.DAOJsonFactory;
import cliente.Servicios.AlumnoService;
import cliente.domainModel.Alumno;
import cliente.domainModel.NivelDeEstudio;

public class main2 {

	public static void main(String[] args) 
	{
		Alumno a1 = new Alumno("Daniel2", "56756", "dani2@d.com", NivelDeEstudio.universitario, 
				"UNGS", "Bella Vista", "Probando Json");
		Alumno a2 = new Alumno("Myriam2", "5789789", "myru2@m.com", NivelDeEstudio.universitario, 
				"UNGS", "Pacheco", "Probando Json2");
		
		DAOJsonFactory daoJsonFactory = new DAOJsonFactory();
		AlumnoService alumnoService = new AlumnoService(daoJsonFactory);
		alumnoService.guardar(a1);
		alumnoService.guardar(a2);
		
		alumnoService.getAlumnos().toString();
		
	}

}
